<?

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.section/.default/style.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.section/.default/themes/red/style.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.section/.default/script.js');

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.item/.default/style.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.item/.default/script.js');

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.section/top/style.css');

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.product.subscribe/.default/style.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/components/bitrix/catalog.product.subscribe/.default/script.js');