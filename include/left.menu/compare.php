<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="infoBox hidden compsre-menu-left">
    <div class="buy_button" id="compareInfoBlock">Сейчас на <a
            href="/catalog/compare/" id="compareInfo">сравнении</a> <span
            class="nb_goods">1 товар</span>.
    </div>
</div>
<?
if (!defined('TEMPLATES_IS_CATALOG')) {
$APPLICATION->IncludeComponent(
        "bitrix:catalog.compare.list", "", array(
    'IBLOCK_TYPE' => 'catalog',
    'IBLOCK_ID' => '2',
    'NAME' => 'CATALOG_COMPARE_LIST',
    'DETAIL_URL' => '/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
    'COMPARE_URL' => '/catalog/compare/',
    'ACTION_VARIABLE' => 'action_ccl',
    'PRODUCT_ID_VARIABLE' => 'id',
    'POSITION_FIXED' => 'N',
    'POSITION' => 'bottom left',
));
}
?>