$(function () {
    'use strict';

    var setCookie = function (key, value) {
        $.cookie('sort-view-' + key, value, {expires: 45, path: '/'});
    }

    $(document).on('change', '.sort-view-change', function () {
        let val = $(this).val();
        setCookie($(this).attr('name'), $(this).find('option[value="' + val + '"]').data('value'));
        window.location.href = val;
    });

    $(document).on('click', '.sort-view-click', function () {
        let val = $(this).val();
        setCookie($(this).data('name'), $(this).data('value'));
        return true;
    });
});