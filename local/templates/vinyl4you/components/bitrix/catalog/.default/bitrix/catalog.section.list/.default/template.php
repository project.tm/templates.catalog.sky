<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID']) {
    $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
    ?>
    <div class="box htmlDataBlock ">
        <div class="box-content">
            <h1>
                <?= htmlspecialcharsback($arParams['arCurSection']['UF_NOTICE']) ?>
            </h1>
        </div>
    </div>
    <?
}
?>
<div class="product-list2 catalog-sections-list">
    <? if ($arResult["SECTIONS_COUNT"]) { ?>
        <? foreach ($arResult['SECTIONS'] as &$arSection) { ?>
            <?
            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
            if (false === $arSection['PICTURE'])
                $arSection['PICTURE'] = array(
                    'SRC' => $arCurView['EMPTY_IMG'],
                    'ALT' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"] ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"] : $arSection["NAME"]
                    ),
                    'TITLE' => (
                    '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"] ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"] : $arSection["NAME"]
                    )
                );

            ?>
            <div class="main_general">
                <div>
                    <div class="product">
                        <div class="product-image-wrapper goods-cat-image-medium-square">
                            <a class="product_img" href="<? echo $arSection['SECTION_PAGE_URL']; ?>" title="<? echo $arSection['PICTURE']['TITLE']; ?>">
                                <img class="goods-cat-image-medium" alt="<? echo $arSection['PICTURE']['TITLE']; ?>" title="<? echo $arSection['PICTURE']['TITLE']; ?>" src="<? echo $arSection['PICTURE']['SRC']; ?>">
                            </a>
                        </div>
                        <div class="product-name">
                            <a class="category-name" href="<? echo $arSection['SECTION_PAGE_URL']; ?>" title="<? echo $arSection['PICTURE']['TITLE']; ?>"><? echo $arSection['NAME']; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        <? } ?>
    <? } ?>
</div>

<? if($arResult['SECTION']['ID']) { ?>
    <br clear="all">
    <div class="htmlDataBlock">
        <?=$arResult['SECTION']['DESCRIPTION'] ?>
    </div>
<? } ?>