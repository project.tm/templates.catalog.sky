<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


if (Bitrix\Main\Loader::includeModule('project.core')) {
    foreach ($arResult['SECTIONS'] as $key=>&$arSection) {
        if(empty($arSection['ELEMENT_CNT'])) {
            unset($arResult['SECTIONS'][$key]);
        } else {
            $arSection['PICTURE']['SRC'] = \Project\Core\Image::resize($arSection['PICTURE']['ID'], 200, 186);
        }
    }
}