<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
$this->setFrameMode(true);

if($arResult['ITEMS']) {
    $listId = array_keys($arResult['ITEMS']);
    $APPLICATION->IncludeComponent('project:product.list', 'top', array(
        'HEADER' => 'С этим товаром покупают',
        'FILTER' => array(
            'ID' => $listId
        )
    ));
}
