<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
$frame = $this->createFrame()->begin();

if (!empty($arResult['ITEMS'])) {
    $listId = array_keys($arResult['ITEMS']);
    if ($listId) {
        $APPLICATION->IncludeComponent('project:product.list', 'viewed', array(
            'HEADER' => 'Просмотренные товары',
            'FILTER' => array(
                'ID' => $listId
            )
        ));
    }
}
?>
<? $frame->beginStub(); ?>
<?

$frame->end();
