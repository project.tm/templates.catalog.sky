<?

if (Bitrix\Main\Loader::includeModule('project.core')) {
    $itemResize = function(&$item) {
        if ($item['ID']) {
            $item['SRC'] = Project\Core\Image::catalog($item['ID'], 54, 54);
        }
    };
    $itemResize($arResult['ITEM']['PREVIEW_PICTURE']);
}

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_ADD_TO_BASKET'] = 'Быстрый заказ';

$fixOldPrice = function(&$item, $old) {
    if ($old) {
        $price = &$item['ITEM_PRICES'][0];
        $price['PERCENT'] = round(($old - $price['RATIO_PRICE']) / $old * 100, 0);
        $price['RATIO_BASE_PRICE'] = $price['BASE_PRICE'] = $old;
        $price['PRINT_BASE_PRICE'] = $price['PRINT_RATIO_BASE_PRICE'] = CCurrencyLang::CurrencyFormat($price['RATIO_PRICE'], $price['CURRENCY']);
    }
};

$haveOffers = !empty($arResult['ITEM']['OFFERS']);
if ($haveOffers) {
    $price = $index = 0;
    foreach ($arResult['ITEM']['OFFERS'] as $key => &$arItem) {
        $priceItem = $arItem['ITEM_PRICES'][0]['BASE_PRICE'];
        if (empty($price) or $price > $priceItem) {
            $price = $priceItem;
            $arResult['ITEM']['OFFERS_SELECTED'] = $key;
        }
        $old = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'];
        $fixOldPrice($arItem, $old);
        $fixOldPrice($arResult['ITEM']['JS_OFFERS'][$key], $old);
    }
}