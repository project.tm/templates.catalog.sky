$(function () {
    'use strict';
    
    $('.catalog-section.catalog-section-top .main_general').mouseenter(function() {
        let index = 0, count = 0;
        let el = $(this);
        let id = el.find('.product-item-big-card').data('id');
        el.parent().find('li.main_general:visible').each(function() {
            index++;
            if(id==$(this).find('.product-item-big-card').data('id')) {
                count = index;
            }
        });
        if(count==4 || index==count) {
//            console.log(count, index, index-count, 'removeClass');
            el.find('.product-item-hidden_left').removeClass('product-item-hidden_rigth');
        } else {
//            console.log(count, index, index-count, 'addClass');
            el.find('.product-item-hidden_left').addClass('product-item-hidden_rigth');
        }
    })
});