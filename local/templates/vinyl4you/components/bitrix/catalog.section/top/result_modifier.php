<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$arResult['ITEM_LIST'] = array();
foreach ($arResult['ITEMS'] as $item) {
    $arResult['ITEM_LIST'][] = $item['ID'];
}
$component->setResultCacheKeys(array(
    "ITEM_LIST"
));
