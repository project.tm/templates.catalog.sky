<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
/** @global CMain $APPLICATION */
$frame = $this->createFrame()->begin("");
$injectId = $arParams['UNIQ_COMPONENT_ID'];

if (isset($arResult['REQUEST_ITEMS'])) {
    // code to receive recommendations from the cloud
    CJSCore::Init(array('ajax'));

    // component parameters
    $signer = new \Bitrix\Main\Security\Sign\Signer;
    $signedParameters = $signer->sign(
            base64_encode(serialize($arResult['_ORIGINAL_PARAMS'])), 'bx.bd.products.recommendation'
    );
    $signedTemplate = $signer->sign($arResult['RCM_TEMPLATE'], 'bx.bd.products.recommendation');
    ?>
    <span id="<?= $injectId ?>"></span>
    <script type="text/javascript">
        BX.ready(function () {
            bx_rcm_get_from_cloud(
                    '<?= CUtil::JSEscape($injectId) ?>',
    <?= CUtil::PhpToJSObject($arResult['RCM_PARAMS']) ?>,
                    {
                        'parameters': '<?= CUtil::JSEscape($signedParameters) ?>',
                        'template': '<?= CUtil::JSEscape($signedTemplate) ?>',
                        'site_id': '<?= CUtil::JSEscape(SITE_ID) ?>',
                        'rcm': 'yes'
                    }
            );
        });
    </script>
    <?
    $frame->end();
    return;
    // \ end of the code to receive recommendations from the cloud
}

//pre($arResult['ITEMS']);
// regular template then
// if customized template, for better js performance don't forget to frame content with <span id="{$injectId}_items">...</span> 

$listId = array_keys($arResult['ITEMS']);
if ($listId) {
    $APPLICATION->IncludeComponent('project:product.list', 'top', array(
        'HEADER' => 'С этим товаром смотрят',
        'FILTER' => array(
            'ID' => $listId
        )
    ));
    ?>
    <script>
        jcarouselInit();
    </script>
    <?
}
$frame->end();
