<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//$fixOldPrice = function(&$item, $old) {
//    if ($old) {
//        $price = &$item['ITEM_PRICES'][0];
//        $price['PERCENT'] = round(($old - $price['RATIO_PRICE']) / $old * 100, 0);
//        $price['RATIO_BASE_PRICE'] = $price['BASE_PRICE'] = $old;
//        $price['PRINT_BASE_PRICE'] = $price['PRINT_RATIO_BASE_PRICE'] = CCurrencyLang::CurrencyFormat($price['RATIO_PRICE'], $price['CURRENCY']);
//    }
//};
//
//$haveOffers = !empty($arResult['OFFERS']);
//if ($haveOffers) {
//    $props = array();
//    foreach ($arResult['JS_OFFERS'] as $offer) {
//        if (isset($offer['TREE']['PROP_32']) and isset($offer['TREE']['PROP_38'])) {
//            if ($offer['TREE']['PROP_38'] != 38) {
//                $props[$offer['TREE']['PROP_32']] = $offer['ITEM_PRICES'][0]['RATIO_PRICE'];
//            }
//        }
//    }
//
//    foreach ($arResult['JS_OFFERS'] as $key => $offer) {
//        if (isset($offer['TREE']['PROP_32']) and isset($offer['TREE']['PROP_38'])) {
//            if ($offer['TREE']['PROP_38'] == 38) {
//                $old = $props[$offer['TREE']['PROP_32']];
//
//                $fixOldPrice($arResult['OFFERS'][$key], $old);
//                $fixOldPrice($arResult['JS_OFFERS'][$key], $old);
//            }
//        }
//    }
//}
