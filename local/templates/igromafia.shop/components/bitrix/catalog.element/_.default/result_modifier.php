<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
$sku_tree = array();
$platforms = array();

 foreach ($arResult['OFFERS'] as $offer)
 {
	 if($offer['PROPERTIES']['M_PLATFORM']['VALUE'] !='' ) 
	 {	 
		 $sku_tree[$offer['PROPERTIES']['M_PLATFORM']['VALUE']][] = $offer['ID'];
		 if($offer['PROPERTIES']['M_WEBVERSION']['VALUE'] == 'да')
		 {
			$sku_tree[$offer['PROPERTIES']['M_PLATFORM']['VALUE']]['WEB'] = $offer['ID'];
		 }
	
	 }
 }
 
 $arResult['SKUTREE'] = $sku_tree;
 
 
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();