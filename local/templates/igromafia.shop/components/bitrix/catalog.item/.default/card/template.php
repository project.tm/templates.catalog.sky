<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<div id="sku-list-order-<?= $item['ID'] ?>" data-obj="<?= $obName ?>">
    <a class="">
        <span class="hidden" id="<?= $itemIds['PICT_SLIDER'] ?>"></span>
        <span class="hidden" id="<?= $itemIds['PICT'] ?>"></span>
        <?
        if ($item['SECOND_PICT']) {
            ?>
            <span class="hidden" id="<?= $itemIds['SECOND_PICT'] ?>">
            </span>
            <?
        }
        if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {
            ?>
            <div class="hidden" id="<?= $itemIds['DSC_PERC'] ?>">
                <span><?= -$price['PERCENT'] ?>%</span>
            </div>
            <?
        }
        if ($item['LABEL']) {
            ?>
            <div class="hidden" id="<?= $itemIds['STICKER_ID'] ?>">
            </div>
            <?
        }
        ?>
        <div class="hidden" id="<?= $itemIds['PICT_SLIDER'] ?>_indicator"></div>
    </a>
    <?
    if (!empty($arParams['PRODUCT_BLOCKS_ORDER'])) {
        foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName) {
            switch ($blockName) {
                case 'price':
                    ?>
                    <?
                    if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
                        ?>
                        <span id="<?= $itemIds['PRICE_OLD'] ?>"
                              <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
                                  <?= $price['PRINT_RATIO_BASE_PRICE'] ?>
                        </span>&nbsp;
                        <?
                    }
                    ?>
                    <span id="<?= $itemIds['PRICE'] ?>">
                        <?
                        if (!empty($price)) {
                            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                                echo Loc::getMessage(
                                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE', array(
                                    '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                    '#VALUE#' => $measureRatio,
                                    '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                        )
                                );
                            } else {
                                echo $price['PRINT_RATIO_PRICE'];
                            }
                        }
                        ?>
                    </span>
                    <?
                    break;

                case 'buttons':
                    if (!$haveOffers) {
                        if ($actualItem['CAN_BUY']) {
                            ?>
                            <div id="<?= $itemIds['BASKET_ACTIONS'] ?>">
                                <a id="<?= $itemIds['BUY_LINK'] ?>"rel="nofollow">
                                </a>
                            </div>
                            <?
                        }
                    } else {
                        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                            ?>
                            <a id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>"></a>
                            <div id="<?= $itemIds['BASKET_ACTIONS'] ?>">
                                <a id="<?= $itemIds['BUY_LINK'] ?>"></a>
                            </div>
                            <?
                        }
                    }
                    break;

                case 'sku':
                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP'])) {
                        ?>
                        <div id="<?= $itemIds['PROP_DIV'] ?>">
                            <?
                            foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                                $propertyId = $skuProperty['ID'];
                                $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                                if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                                    continue;
                                ?>
                                <ul class="product-item-scu-item-list">
                                    <?
                                    foreach ($skuProperty['VALUES'] as $value) {
                                        if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                            continue;

                                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                        if ($skuProperty['SHOW_MODE'] === 'PICT') {
                                            ?>
                                            <li class="product-item-scu-item-color-container" title="<?= $value['NAME'] ?>"
                                                id="prors-<?= $item['ID'] ?>-<?= $value['ID'] ?>"
                                                data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>" data-onevalue="<?= $value['ID'] ?>">
                                                <div class="product-item-scu-item-color-block">
                                                    <div class="product-item-scu-item-color" title="<?= $value['NAME'] ?>"
                                                         style="background-image: url(<?= $value['PICT']['SRC'] ?>);">
                                                    </div>
                                                </div>
                                            </li>
                                            <?
                                        } else {
                                            ?>
                                            <li class="product-item-scu-item-text-container" title="<?= $value['NAME'] ?>"
                                                id="prors-<?= $item['ID'] ?>-<?= $value['ID'] ?>"
                                                data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>" data-onevalue="<?= $value['ID'] ?>">
                                                <div class="product-item-scu-item-text-block">
                                                    <div class="product-item-scu-item-text"><?= $value['ID'] ?> <?= $value['NAME'] ?></div>
                                                </div>
                                            </li>
                                            <?
                                        }
                                    }
                                    ?>
                                </ul>
                                <?
                            }
                            ?>
                        </div>
                        <?
                        foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                            if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                                continue;

                            $skuProps[] = array(
                                'ID' => $skuProperty['ID'],
                                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                'VALUES' => $skuProperty['VALUES'],
                                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                            );
                        }
                        unset($skuProperty, $value);
                        if ($item['OFFERS_PROPS_DISPLAY']) {
                            foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer) {
                                $strProps = '';

                                if (!empty($jsOffer['DISPLAY_PROPERTIES'])) {
                                    foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty) {
                                        $strProps .= '<dt>' . $displayProperty['NAME'] . '</dt><dd>'
                                                . (is_array($displayProperty['VALUE']) ? implode(' / ', $displayProperty['VALUE']) : $displayProperty['VALUE'])
                                                . '</dd>';
                                    }
                                }

                                $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
                            }
                            unset($jsOffer, $strProps);
                        }
                    }

                    break;
            }
        }
    }
    ?>
</div>