<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
$this->setFrameMode(true);
?>
<?
if (!empty($arResult['ITEMS'])) {
    ?>
    <div class="wrap-games row">
        <?
        foreach ($arResult['ITEMS'] as $arItem) {
            //print_r($arItem);
            ?>
            <div class="btrp-col col-lg-3 col-md-4 col-sm-6 col-xs-12 games-section-item">
                <div class="games">
                    <a class="img" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" style="background: url(<? if ($arItem['PREVIEW_PICTURE']['SRC'] != '') { ?><?= $arItem['PREVIEW_PICTURE']['SRC'] ?><? } else { ?>/local/templates/shop/components/bitrix/catalog.element/.default/images/no_photo.png<? } ?>);"></a>
                    <div class="info-game">
                        <a class="name" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?>
                        </a>
                        <div class="platforms">
                            <?
                            $ar_console = array();
                            foreach ($arItem['OFFERS'] as $offer) {
                                if (!in_array($offer['PROPERTIES']['M_PLATFORM']['VALUE'], $ar_console)) {
                                    $ar_console[] = $offer['PROPERTIES']['M_PLATFORM']['VALUE'];
                                    ?>
                                    <div class="console"><?= $offer['PROPERTIES']['M_PLATFORM']['VALUE'] ?></div>
                                    <?
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="wrap-absolute">
                        <?
                        foreach ($arItem['OFFERS'] as $offer) {
                            if ($offer['PROPERTIES']['M_WEBVERSION']['VALUE'] != TEMPLATES_SKY_WEBVERSION) {
                                ?>
                                <div class="prise">
                                    <div class="current-platform"><span><?= $offer['PROPERTIES']['M_PLATFORM']['VALUE'] ?></span></div>
                                    <div class="money">
                                        <?= $offer['ITEM_PRICES'][0]['BASE_PRICE'] ?><span class="small-rub">р.</span>
                                    </div>
                                    <a class="buy list-sky-basket" data-id="<?=$arItem['ID'] ?>" data-offers="<?=$arItem['ID'] ?>-<?=$offer['PROPERTIES']['M_PLATFORM']['VALUE_ENUM_ID'] ?>" href="<?= $offer['~BUY_URL'] ?>"></a>
                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="align-wrap">
                    <div class="delete-game" data-id="<?= $arItem['ID'] ?>">убрать из избранного</div>
                </div>
            </div>
        <? } ?>
    </div>
    <div>
        <!-- pagination-container -->
        <?= $arResult['NAV_STRING'] ?>
        <!-- pagination-container -->
    </div>
    <? include(__DIR__ .'/../.default/template.v2.php');
} else {
    if (!empty($_REQUEST['PAGEN_1']) and $_REQUEST['PAGEN_1'] > 1) {
        ?>
        <script>
        <? if ($_REQUEST['PAGEN_1'] > 2) { ?>
                window.location.href = '<?= $arParams['PAGER_BASE_LINK'] ?>?PAGEN_1=<?= $_REQUEST['PAGEN_1'] - 1 ?>';
        <? } elseif ($_REQUEST['PAGEN_1'] == 2) { ?>
                    window.location.href = '<?= $arParams['PAGER_BASE_LINK'] ?>';
        <? } ?>
        </script>
        <?
    }
    ?>
    <div class="wrap-games row"><div style="margin: 0px auto; color: #fff; padding:10px;">Нет результатов</div></div>
<? }
?>