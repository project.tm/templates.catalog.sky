# Шаблоны для магазина #

## Шаблон [/local/templates/vinyl4you/components/bitrix/](https://bitbucket.org/project-tm/templates.catalog.sky/src/4a609c790c4bc1a260a992902c4f4d743926eaff/local/templates/vinyl4you/components/bitrix/) ##

* выбор товарных предложений через select
* c этим товаров смотрят, покупают
* блок заказываемых, рекомендованных товаров
* сравнение
* избранное
* просмотренные товары
* формы

![screen.png](https://bitbucket.org/repo/MrnXxL7/images/3854235771-screen.png)
![screen2.png](https://bitbucket.org/repo/MrnXxL7/images/3212516627-screen2.png)


## Шаблон [/local/templates/shop/components/bitrix/](https://bitbucket.org/project-tm/templates.catalog.sky/src/4a609c790c4bc1a260a992902c4f4d743926eaff/local/templates/shop/components/bitrix/) ##

* Товарные предложения (цифровая, не цифровая) сделаны чекбоксом
* Не цифровая версия - старая цена, для цифровой версии

![screen3.png](https://bitbucket.org/repo/MrnXxL7/images/2672135757-screen3.png)
![screen4.png](https://bitbucket.org/repo/MrnXxL7/images/1696089231-screen4.png)